﻿namespace CarRental
{
    partial class RentalManagement_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Carid = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Clientid = new System.Windows.Forms.Label();
            this.Returnid = new System.Windows.Forms.Label();
            this.Pickupid = new System.Windows.Forms.Label();
            this.discount = new System.Windows.Forms.Label();
            this.Penalty = new System.Windows.Forms.Label();
            this.SDate = new System.Windows.Forms.Label();
            this.EDate = new System.Windows.Forms.Label();
            this.CDate = new System.Windows.Forms.Label();
            this.textBoxCarid = new System.Windows.Forms.TextBox();
            this.textClientid = new System.Windows.Forms.TextBox();
            this.textPickupid = new System.Windows.Forms.TextBox();
            this.textReturnid = new System.Windows.Forms.TextBox();
            this.checkedDiscount = new System.Windows.Forms.CheckedListBox();
            this.comboPenalty = new System.Windows.Forms.ComboBox();
            this.dateTimeCreation = new System.Windows.Forms.DateTimePicker();
            this.dateTimeEnd = new System.Windows.Forms.DateTimePicker();
            this.dateTimeStart = new System.Windows.Forms.DateTimePicker();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Filter = new System.Windows.Forms.Button();
            this.New = new System.Windows.Forms.Button();
            this.update = new System.Windows.Forms.Button();
            this.delete = new System.Windows.Forms.Button();
            this.exit = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // Carid
            // 
            this.Carid.AutoSize = true;
            this.Carid.Location = new System.Drawing.Point(55, 38);
            this.Carid.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Carid.Name = "Carid";
            this.Carid.Size = new System.Drawing.Size(40, 13);
            this.Carid.TabIndex = 0;
            this.Carid.Text = "Car ID:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(285, 7);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Car Deals";
            // 
            // Clientid
            // 
            this.Clientid.AutoSize = true;
            this.Clientid.Location = new System.Drawing.Point(45, 69);
            this.Clientid.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Clientid.Name = "Clientid";
            this.Clientid.Size = new System.Drawing.Size(50, 13);
            this.Clientid.TabIndex = 2;
            this.Clientid.Text = "Client ID:";
            this.Clientid.Click += new System.EventHandler(this.label3_Click);
            // 
            // Returnid
            // 
            this.Returnid.AutoSize = true;
            this.Returnid.Location = new System.Drawing.Point(9, 141);
            this.Returnid.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Returnid.Name = "Returnid";
            this.Returnid.Size = new System.Drawing.Size(87, 13);
            this.Returnid.TabIndex = 3;
            this.Returnid.Text = "Return Office ID:";
            // 
            // Pickupid
            // 
            this.Pickupid.AutoSize = true;
            this.Pickupid.Location = new System.Drawing.Point(9, 102);
            this.Pickupid.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Pickupid.Name = "Pickupid";
            this.Pickupid.Size = new System.Drawing.Size(88, 13);
            this.Pickupid.TabIndex = 4;
            this.Pickupid.Text = "Pickup Office ID:";
            this.Pickupid.Click += new System.EventHandler(this.label5_Click);
            // 
            // discount
            // 
            this.discount.AutoSize = true;
            this.discount.Location = new System.Drawing.Point(196, 38);
            this.discount.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.discount.Name = "discount";
            this.discount.Size = new System.Drawing.Size(52, 13);
            this.discount.TabIndex = 5;
            this.discount.Text = "Discount:";
            // 
            // Penalty
            // 
            this.Penalty.AutoSize = true;
            this.Penalty.Location = new System.Drawing.Point(206, 105);
            this.Penalty.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Penalty.Name = "Penalty";
            this.Penalty.Size = new System.Drawing.Size(45, 13);
            this.Penalty.TabIndex = 6;
            this.Penalty.Text = "Penalty:";
            // 
            // SDate
            // 
            this.SDate.AutoSize = true;
            this.SDate.Location = new System.Drawing.Point(196, 141);
            this.SDate.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.SDate.Name = "SDate";
            this.SDate.Size = new System.Drawing.Size(58, 13);
            this.SDate.TabIndex = 7;
            this.SDate.Text = "Start Date:";
            // 
            // EDate
            // 
            this.EDate.AutoSize = true;
            this.EDate.Location = new System.Drawing.Point(394, 38);
            this.EDate.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.EDate.Name = "EDate";
            this.EDate.Size = new System.Drawing.Size(55, 13);
            this.EDate.TabIndex = 8;
            this.EDate.Text = "End Date:";
            // 
            // CDate
            // 
            this.CDate.AutoSize = true;
            this.CDate.Location = new System.Drawing.Point(373, 73);
            this.CDate.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.CDate.Name = "CDate";
            this.CDate.Size = new System.Drawing.Size(75, 13);
            this.CDate.TabIndex = 9;
            this.CDate.Text = "Creation Date:";
            // 
            // textBoxCarid
            // 
            this.textBoxCarid.Location = new System.Drawing.Point(98, 34);
            this.textBoxCarid.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBoxCarid.Name = "textBoxCarid";
            this.textBoxCarid.Size = new System.Drawing.Size(76, 20);
            this.textBoxCarid.TabIndex = 14;
            // 
            // textClientid
            // 
            this.textClientid.Location = new System.Drawing.Point(98, 69);
            this.textClientid.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textClientid.Name = "textClientid";
            this.textClientid.Size = new System.Drawing.Size(76, 20);
            this.textClientid.TabIndex = 15;
            // 
            // textPickupid
            // 
            this.textPickupid.Location = new System.Drawing.Point(98, 102);
            this.textPickupid.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textPickupid.Name = "textPickupid";
            this.textPickupid.Size = new System.Drawing.Size(76, 20);
            this.textPickupid.TabIndex = 16;
            // 
            // textReturnid
            // 
            this.textReturnid.Location = new System.Drawing.Point(98, 137);
            this.textReturnid.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textReturnid.Name = "textReturnid";
            this.textReturnid.Size = new System.Drawing.Size(76, 20);
            this.textReturnid.TabIndex = 17;
            // 
            // checkedDiscount
            // 
            this.checkedDiscount.FormattingEnabled = true;
            this.checkedDiscount.Items.AddRange(new object[] {
            "5% for 3 hours",
            "10% for 5 hours",
            "25% for 8 hours",
            "35% for 24 hours",
            "40% for 48 hours"});
            this.checkedDiscount.Location = new System.Drawing.Point(259, 28);
            this.checkedDiscount.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkedDiscount.Name = "checkedDiscount";
            this.checkedDiscount.Size = new System.Drawing.Size(91, 49);
            this.checkedDiscount.TabIndex = 18;
            // 
            // comboPenalty
            // 
            this.comboPenalty.FormattingEnabled = true;
            this.comboPenalty.Items.AddRange(new object[] {
            "5$ for one hour",
            "15$ for two hours",
            "30$ for three hours",
            "45$ for four hours",
            "price*2 after four hours"});
            this.comboPenalty.Location = new System.Drawing.Point(259, 101);
            this.comboPenalty.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comboPenalty.Name = "comboPenalty";
            this.comboPenalty.Size = new System.Drawing.Size(91, 21);
            this.comboPenalty.TabIndex = 19;
            // 
            // dateTimeCreation
            // 
            this.dateTimeCreation.Location = new System.Drawing.Point(461, 69);
            this.dateTimeCreation.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dateTimeCreation.Name = "dateTimeCreation";
            this.dateTimeCreation.Size = new System.Drawing.Size(92, 20);
            this.dateTimeCreation.TabIndex = 20;
            this.dateTimeCreation.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // dateTimeEnd
            // 
            this.dateTimeEnd.Location = new System.Drawing.Point(461, 34);
            this.dateTimeEnd.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dateTimeEnd.Name = "dateTimeEnd";
            this.dateTimeEnd.Size = new System.Drawing.Size(92, 20);
            this.dateTimeEnd.TabIndex = 21;
            this.dateTimeEnd.ValueChanged += new System.EventHandler(this.dateTimePicker2_ValueChanged);
            // 
            // dateTimeStart
            // 
            this.dateTimeStart.Location = new System.Drawing.Point(259, 137);
            this.dateTimeStart.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dateTimeStart.Name = "dateTimeStart";
            this.dateTimeStart.Size = new System.Drawing.Size(92, 20);
            this.dateTimeStart.TabIndex = 22;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column,
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6,
            this.Column7,
            this.Column8});
            this.dataGridView1.Location = new System.Drawing.Point(11, 165);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(706, 333);
            this.dataGridView1.TabIndex = 23;
            // 
            // Column
            // 
            this.Column.HeaderText = "Car ID:";
            this.Column.Name = "Column";
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Client ID:";
            this.Column1.Name = "Column1";
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Pickup Office ID:";
            this.Column2.Name = "Column2";
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Return Office ID:";
            this.Column3.Name = "Column3";
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Discount";
            this.Column4.Name = "Column4";
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Penalty";
            this.Column5.Name = "Column5";
            // 
            // Column6
            // 
            this.Column6.HeaderText = "Start Date:";
            this.Column6.Name = "Column6";
            // 
            // Column7
            // 
            this.Column7.HeaderText = "End Date:";
            this.Column7.Name = "Column7";
            // 
            // Column8
            // 
            this.Column8.HeaderText = "Creation Date:";
            this.Column8.Name = "Column8";
            // 
            // Filter
            // 
            this.Filter.Location = new System.Drawing.Point(482, 119);
            this.Filter.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Filter.Name = "Filter";
            this.Filter.Size = new System.Drawing.Size(70, 19);
            this.Filter.TabIndex = 24;
            this.Filter.Text = "Filter";
            this.Filter.UseVisualStyleBackColor = true;
            // 
            // New
            // 
            this.New.Location = new System.Drawing.Point(33, 510);
            this.New.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.New.Name = "New";
            this.New.Size = new System.Drawing.Size(61, 26);
            this.New.TabIndex = 25;
            this.New.Text = "New";
            this.New.UseVisualStyleBackColor = true;
            this.New.Click += new System.EventHandler(this.New_Click);
            // 
            // update
            // 
            this.update.Location = new System.Drawing.Point(175, 511);
            this.update.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.update.Name = "update";
            this.update.Size = new System.Drawing.Size(56, 26);
            this.update.TabIndex = 26;
            this.update.Text = "Update";
            this.update.UseVisualStyleBackColor = true;
            // 
            // delete
            // 
            this.delete.Location = new System.Drawing.Point(105, 511);
            this.delete.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.delete.Name = "delete";
            this.delete.Size = new System.Drawing.Size(56, 25);
            this.delete.TabIndex = 27;
            this.delete.Text = "Delete";
            this.delete.UseVisualStyleBackColor = true;
            // 
            // exit
            // 
            this.exit.Location = new System.Drawing.Point(632, 511);
            this.exit.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.exit.Name = "exit";
            this.exit.Size = new System.Drawing.Size(62, 25);
            this.exit.TabIndex = 28;
            this.exit.Text = "Exit";
            this.exit.UseVisualStyleBackColor = true;
            this.exit.Click += new System.EventHandler(this.exit_Click);
            // 
            // RentalManagement_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(722, 546);
            this.Controls.Add(this.exit);
            this.Controls.Add(this.delete);
            this.Controls.Add(this.update);
            this.Controls.Add(this.New);
            this.Controls.Add(this.Filter);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.dateTimeStart);
            this.Controls.Add(this.dateTimeEnd);
            this.Controls.Add(this.dateTimeCreation);
            this.Controls.Add(this.comboPenalty);
            this.Controls.Add(this.checkedDiscount);
            this.Controls.Add(this.textReturnid);
            this.Controls.Add(this.textPickupid);
            this.Controls.Add(this.textClientid);
            this.Controls.Add(this.textBoxCarid);
            this.Controls.Add(this.CDate);
            this.Controls.Add(this.EDate);
            this.Controls.Add(this.SDate);
            this.Controls.Add(this.Penalty);
            this.Controls.Add(this.discount);
            this.Controls.Add(this.Pickupid);
            this.Controls.Add(this.Returnid);
            this.Controls.Add(this.Clientid);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Carid);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "RentalManagement_Form";
            this.Text = "RentalManagement_Form";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Carid;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label Clientid;
        private System.Windows.Forms.Label Returnid;
        private System.Windows.Forms.Label Pickupid;
        private System.Windows.Forms.Label discount;
        private System.Windows.Forms.Label Penalty;
        private System.Windows.Forms.Label SDate;
        private System.Windows.Forms.Label EDate;
        private System.Windows.Forms.Label CDate;
        private System.Windows.Forms.TextBox textBoxCarid;
        private System.Windows.Forms.TextBox textClientid;
        private System.Windows.Forms.TextBox textPickupid;
        private System.Windows.Forms.TextBox textReturnid;
        private System.Windows.Forms.CheckedListBox checkedDiscount;
        private System.Windows.Forms.ComboBox comboPenalty;
        private System.Windows.Forms.DateTimePicker dateTimeCreation;
        private System.Windows.Forms.DateTimePicker dateTimeEnd;
        private System.Windows.Forms.DateTimePicker dateTimeStart;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.Button Filter;
        private System.Windows.Forms.Button New;
        private System.Windows.Forms.Button update;
        private System.Windows.Forms.Button delete;
        private System.Windows.Forms.Button exit;
    }
}