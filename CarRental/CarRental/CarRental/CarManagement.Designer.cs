﻿namespace CarRental
{
    partial class CarManagement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupFilter = new System.Windows.Forms.GroupBox();
            this.lblYearTo = new System.Windows.Forms.Label();
            this.txtYearFrom = new System.Windows.Forms.TextBox();
            this.lblMileageTo = new System.Windows.Forms.Label();
            this.txtMileageTo = new System.Windows.Forms.TextBox();
            this.comboBoxCar = new System.Windows.Forms.ComboBox();
            this.btnFilter = new System.Windows.Forms.Button();
            this.lblYear = new System.Windows.Forms.Label();
            this.txtYearTo = new System.Windows.Forms.TextBox();
            this.comboBoxCarType = new System.Windows.Forms.ComboBox();
            this.lblType = new System.Windows.Forms.Label();
            this.lblMileage = new System.Windows.Forms.Label();
            this.txtMileageFrom = new System.Windows.Forms.TextBox();
            this.comboBoxCarModel = new System.Windows.Forms.ComboBox();
            this.lblModel = new System.Windows.Forms.Label();
            this.lblBrandName = new System.Windows.Forms.Label();
            this.comboBoxCarBrand = new System.Windows.Forms.ComboBox();
            this.lblCarName = new System.Windows.Forms.Label();
            this.dataGridViewCar = new System.Windows.Forms.DataGridView();
            this.btnNew = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.groupFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCar)).BeginInit();
            this.SuspendLayout();
            // 
            // groupFilter
            // 
            this.groupFilter.Controls.Add(this.lblYearTo);
            this.groupFilter.Controls.Add(this.txtYearFrom);
            this.groupFilter.Controls.Add(this.lblMileageTo);
            this.groupFilter.Controls.Add(this.txtMileageTo);
            this.groupFilter.Controls.Add(this.comboBoxCar);
            this.groupFilter.Controls.Add(this.btnFilter);
            this.groupFilter.Controls.Add(this.lblYear);
            this.groupFilter.Controls.Add(this.txtYearTo);
            this.groupFilter.Controls.Add(this.comboBoxCarType);
            this.groupFilter.Controls.Add(this.lblType);
            this.groupFilter.Controls.Add(this.lblMileage);
            this.groupFilter.Controls.Add(this.txtMileageFrom);
            this.groupFilter.Controls.Add(this.comboBoxCarModel);
            this.groupFilter.Controls.Add(this.lblModel);
            this.groupFilter.Controls.Add(this.lblBrandName);
            this.groupFilter.Controls.Add(this.comboBoxCarBrand);
            this.groupFilter.Controls.Add(this.lblCarName);
            this.groupFilter.Location = new System.Drawing.Point(12, 12);
            this.groupFilter.Name = "groupFilter";
            this.groupFilter.Size = new System.Drawing.Size(603, 128);
            this.groupFilter.TabIndex = 1;
            this.groupFilter.TabStop = false;
            this.groupFilter.Text = "Filter option";
            // 
            // lblYearTo
            // 
            this.lblYearTo.AutoSize = true;
            this.lblYearTo.Location = new System.Drawing.Point(529, 60);
            this.lblYearTo.Name = "lblYearTo";
            this.lblYearTo.Size = new System.Drawing.Size(16, 13);
            this.lblYearTo.TabIndex = 40;
            this.lblYearTo.Text = "to";
            // 
            // txtYearFrom
            // 
            this.txtYearFrom.Location = new System.Drawing.Point(475, 57);
            this.txtYearFrom.Name = "txtYearFrom";
            this.txtYearFrom.Size = new System.Drawing.Size(47, 20);
            this.txtYearFrom.TabIndex = 39;
            // 
            // lblMileageTo
            // 
            this.lblMileageTo.AutoSize = true;
            this.lblMileageTo.Location = new System.Drawing.Point(323, 61);
            this.lblMileageTo.Name = "lblMileageTo";
            this.lblMileageTo.Size = new System.Drawing.Size(16, 13);
            this.lblMileageTo.TabIndex = 38;
            this.lblMileageTo.Text = "to";
            // 
            // txtMileageTo
            // 
            this.txtMileageTo.Location = new System.Drawing.Point(345, 58);
            this.txtMileageTo.Name = "txtMileageTo";
            this.txtMileageTo.Size = new System.Drawing.Size(59, 20);
            this.txtMileageTo.TabIndex = 37;
            // 
            // comboBoxCar
            // 
            this.comboBoxCar.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCar.FormattingEnabled = true;
            this.comboBoxCar.Location = new System.Drawing.Point(75, 25);
            this.comboBoxCar.Name = "comboBoxCar";
            this.comboBoxCar.Size = new System.Drawing.Size(114, 21);
            this.comboBoxCar.TabIndex = 36;
            // 
            // btnFilter
            // 
            this.btnFilter.Location = new System.Drawing.Point(483, 95);
            this.btnFilter.Name = "btnFilter";
            this.btnFilter.Size = new System.Drawing.Size(114, 23);
            this.btnFilter.TabIndex = 35;
            this.btnFilter.Text = "&Filter";
            this.btnFilter.UseVisualStyleBackColor = true;
            this.btnFilter.Click += new System.EventHandler(this.btnFilter_Click);
            // 
            // lblYear
            // 
            this.lblYear.AutoSize = true;
            this.lblYear.Location = new System.Drawing.Point(437, 61);
            this.lblYear.Name = "lblYear";
            this.lblYear.Size = new System.Drawing.Size(32, 13);
            this.lblYear.TabIndex = 34;
            this.lblYear.Text = "Year:";
            // 
            // txtYearTo
            // 
            this.txtYearTo.Location = new System.Drawing.Point(550, 58);
            this.txtYearTo.Name = "txtYearTo";
            this.txtYearTo.Size = new System.Drawing.Size(47, 20);
            this.txtYearTo.TabIndex = 33;
            // 
            // comboBoxCarType
            // 
            this.comboBoxCarType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCarType.FormattingEnabled = true;
            this.comboBoxCarType.Location = new System.Drawing.Point(475, 25);
            this.comboBoxCarType.Name = "comboBoxCarType";
            this.comboBoxCarType.Size = new System.Drawing.Size(122, 21);
            this.comboBoxCarType.TabIndex = 32;
            // 
            // lblType
            // 
            this.lblType.AutoSize = true;
            this.lblType.Location = new System.Drawing.Point(435, 28);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(34, 13);
            this.lblType.TabIndex = 31;
            this.lblType.Text = "Type:";
            // 
            // lblMileage
            // 
            this.lblMileage.AutoSize = true;
            this.lblMileage.Location = new System.Drawing.Point(205, 61);
            this.lblMileage.Name = "lblMileage";
            this.lblMileage.Size = new System.Drawing.Size(47, 13);
            this.lblMileage.TabIndex = 30;
            this.lblMileage.Text = "Mileage:";
            // 
            // txtMileageFrom
            // 
            this.txtMileageFrom.Location = new System.Drawing.Point(258, 58);
            this.txtMileageFrom.Name = "txtMileageFrom";
            this.txtMileageFrom.Size = new System.Drawing.Size(59, 20);
            this.txtMileageFrom.TabIndex = 29;
            // 
            // comboBoxCarModel
            // 
            this.comboBoxCarModel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCarModel.FormattingEnabled = true;
            this.comboBoxCarModel.Location = new System.Drawing.Point(75, 58);
            this.comboBoxCarModel.Name = "comboBoxCarModel";
            this.comboBoxCarModel.Size = new System.Drawing.Size(114, 21);
            this.comboBoxCarModel.TabIndex = 28;
            // 
            // lblModel
            // 
            this.lblModel.AutoSize = true;
            this.lblModel.Location = new System.Drawing.Point(12, 64);
            this.lblModel.Name = "lblModel";
            this.lblModel.Size = new System.Drawing.Size(39, 13);
            this.lblModel.TabIndex = 27;
            this.lblModel.Text = "Model:";
            // 
            // lblBrandName
            // 
            this.lblBrandName.AutoSize = true;
            this.lblBrandName.Location = new System.Drawing.Point(205, 28);
            this.lblBrandName.Name = "lblBrandName";
            this.lblBrandName.Size = new System.Drawing.Size(38, 13);
            this.lblBrandName.TabIndex = 26;
            this.lblBrandName.Text = "Brand:";
            // 
            // comboBoxCarBrand
            // 
            this.comboBoxCarBrand.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCarBrand.FormattingEnabled = true;
            this.comboBoxCarBrand.Location = new System.Drawing.Point(249, 25);
            this.comboBoxCarBrand.Name = "comboBoxCarBrand";
            this.comboBoxCarBrand.Size = new System.Drawing.Size(155, 21);
            this.comboBoxCarBrand.TabIndex = 25;
            // 
            // lblCarName
            // 
            this.lblCarName.AutoSize = true;
            this.lblCarName.Location = new System.Drawing.Point(12, 28);
            this.lblCarName.Name = "lblCarName";
            this.lblCarName.Size = new System.Drawing.Size(57, 13);
            this.lblCarName.TabIndex = 23;
            this.lblCarName.Text = "Car Name:";
            // 
            // dataGridViewCar
            // 
            this.dataGridViewCar.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewCar.Location = new System.Drawing.Point(12, 146);
            this.dataGridViewCar.Name = "dataGridViewCar";
            this.dataGridViewCar.ReadOnly = true;
            this.dataGridViewCar.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewCar.ShowEditingIcon = false;
            this.dataGridViewCar.Size = new System.Drawing.Size(603, 403);
            this.dataGridViewCar.TabIndex = 2;
            // 
            // btnNew
            // 
            this.btnNew.Location = new System.Drawing.Point(18, 569);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(75, 23);
            this.btnNew.TabIndex = 3;
            this.btnNew.Text = "&New";
            this.btnNew.UseVisualStyleBackColor = true;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(99, 569);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 4;
            this.btnDelete.Text = "&Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(180, 569);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(75, 23);
            this.btnUpdate.TabIndex = 5;
            this.btnUpdate.Text = "&Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(540, 569);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 6;
            this.btnExit.Text = "&Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // CarManagement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(627, 612);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnNew);
            this.Controls.Add(this.dataGridViewCar);
            this.Controls.Add(this.groupFilter);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CarManagement";
            this.Text = "Car Management";
            this.Activated += new System.EventHandler(this.CarManagement_Activated);
            this.Load += new System.EventHandler(this.CarManagement_Load);
            this.Shown += new System.EventHandler(this.CarManagement_Shown);
            this.groupFilter.ResumeLayout(false);
            this.groupFilter.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCar)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupFilter;
        private System.Windows.Forms.TextBox txtYearTo;
        private System.Windows.Forms.ComboBox comboBoxCarType;
        private System.Windows.Forms.Label lblType;
        private System.Windows.Forms.Label lblMileage;
        private System.Windows.Forms.TextBox txtMileageFrom;
        private System.Windows.Forms.ComboBox comboBoxCarModel;
        private System.Windows.Forms.Label lblModel;
        private System.Windows.Forms.Label lblBrandName;
        private System.Windows.Forms.ComboBox comboBoxCarBrand;
        private System.Windows.Forms.Label lblCarName;
        private System.Windows.Forms.Label lblYear;
        private System.Windows.Forms.DataGridView dataGridViewCar;
        private System.Windows.Forms.Button btnFilter;
        private System.Windows.Forms.ComboBox comboBoxCar;
        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.TextBox txtYearFrom;
        private System.Windows.Forms.Label lblMileageTo;
        private System.Windows.Forms.TextBox txtMileageTo;
        private System.Windows.Forms.Label lblYearTo;
    }
}