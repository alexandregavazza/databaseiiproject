﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace CarRental.Entity
{
    public class CarType
    {
        public CarType(int id, string description)
        {
            Id = id;
            Description = description;
        }

        [BsonId]
        public ObjectId _id { get; set; }
        public Int32 Id { get; set; }
        public string Description { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
