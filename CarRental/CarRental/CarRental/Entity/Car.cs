﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Data;

namespace CarRental.Entity
{
    public class Car
    {
        public Car() { }

        public Car(int id, string name)
        {
            Id = id;
            Name = name;
        }

        [BsonId]
        public ObjectId _id { get; set; }
        public Int32 Id { get; set; }
        public string Name { get; set; }
        public Int32 ModelId { get; set; }
        public Int32 BrandId { get; set; }
        public float Mileage { get; set; }
        public Int32 TypeId { get; set; }
        public Int32 Year { get; set; }
        public DateTime CreationDate { get; set; }
        public string PlateNumber { get; set; }

        public static implicit operator Car(DataRow v)
        {
            throw new NotImplementedException();
        }
    }
}
