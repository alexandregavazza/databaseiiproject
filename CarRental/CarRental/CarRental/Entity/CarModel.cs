﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace CarRental.Entity
{
    public class CarModel
    {
        public CarModel(int id, string name)
        {
            Id = id;
            Name = name;
        }

        [BsonId]
        public ObjectId _id { get; set; }
        public Int32 Id { get; set; }
        public string Name { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
