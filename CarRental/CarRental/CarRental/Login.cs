﻿using CarRental.Entity;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Configuration;
using System.Linq;
using System.Windows.Forms;

namespace CarRental
{
    public partial class Login : Form
    {
        private IMongoDatabase db;
        private IMongoCollection<User> collection;

        public Login()
        {
            InitializeComponent();
            txtLogin.Focus();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            db = MongoDB.GetDatabase(ConfigurationManager.AppSettings["databaseName"].ToString());
            collection = db.GetCollection<User>("User");

            var filter = new BsonDocument("Login", txtLogin.Text)
                .Add("Password", txtPassword.Text);
            var results = collection.Find(filter).ToList();

            if (results.Count == 1)
            {
                Main main = new Main();
                main.Show();
                this.Hide();
            }
            else
                MessageBox.Show("Login/password don't match!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
