﻿using CarRental.Entity;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CarRental
{
    public partial class CarManagement_Form : Form
    {
        private IMongoDatabase _db;
        private IMongoCollection<Car> _carCollection;
        private IMongoCollection<CarBrand> _carBrandCollection;
        private IMongoCollection<CarType> _carTypeCollection;
        private IMongoCollection<CarModel> _carModelCollection;

        private static readonly string _selectDefaultValue = ConfigurationManager.AppSettings["selectDefaultValue"].ToString();

        public CarManagement_Form()
        {
            InitializeComponent();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            ClearFields();
        }

        private void ClearFields()
        {
            comboBoxBrand.Focus();
            comboBoxBrand.SelectedIndex = 0;
            comboBoxModel.SelectedIndex = 0;
            comboBoxType.SelectedIndex = 0;

            txtMileage.Text = string.Empty;
            txtYear.Text = string.Empty;
            txtPlateNumber.Text = string.Empty;
            txtCarName.Text = string.Empty;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            txtUpdateId.Text = "";
            this.Close();
        }

        private void CarManagement_Form_Load(object sender, EventArgs e)
        {
            _db = MongoDB.GetDatabase(ConfigurationManager.AppSettings["databaseName"].ToString());

            _carCollection = _db.GetCollection<Car>("Car");
            _carBrandCollection = _db.GetCollection<CarBrand>("CarBrand");
            _carTypeCollection = _db.GetCollection<CarType>("CarType");
            _carModelCollection = _db.GetCollection<CarModel>("CarModel");

            LoadCarBrand();
            LoadCarType();
            LoadCarModel();

            if (txtUpdateId.Text != "0" && txtUpdateId.Text.Trim() != string.Empty)
            {
                var carListResult = _carCollection.Find(new BsonDocument("Id", int.Parse(txtUpdateId.Text))).ToList();
                var carSelected = carListResult.First() as Car;

                try
                {
                    comboBoxBrand.SelectedValue = carSelected.BrandId;
                    comboBoxModel.SelectedValue = carSelected.ModelId;
                    comboBoxType.SelectedValue = carSelected.TypeId;
                    txtCarName.Text = carSelected.Name;
                    txtMileage.Text = carSelected.Mileage.ToString();
                    txtYear.Text = carSelected.Year.ToString();
                    txtPlateNumber.Text = carSelected.PlateNumber;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Something went wrong!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void LoadComboBox<T>(ComboBox comboO, List<T> dataSource, string displayMember = "Name", string valueMember = "Id")
        {
            comboO.DataSource = dataSource;
            comboO.ValueMember = valueMember;
            comboO.DisplayMember = displayMember;
        }

        private void LoadCarModel()
        {
            var results = _carModelCollection.Find(new BsonDocument()).Sort(new BsonDocument("Name", 1)).ToList();

            results.Insert(0, new CarModel(0, _selectDefaultValue));
            LoadComboBox(comboBoxModel, results);
        }

        private void LoadCarType()
        {
            var results = _carTypeCollection.Find(new BsonDocument()).Sort(new BsonDocument("Description", 1)).ToList();

            results.Insert(0, new CarType(0, _selectDefaultValue));
            LoadComboBox(comboBoxType, results, "Description");
        }

        private void LoadCarBrand()
        {
            var results = _carBrandCollection.Find(new BsonDocument()).Sort(new BsonDocument("Name", 1)).ToList();

            results.Insert(0, new CarBrand(0, _selectDefaultValue));
            LoadComboBox(comboBoxBrand, results);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                var carList = _carCollection.Find(new BsonDocument()).Sort(new BsonDocument("Id", -1)).ToList();
                Car latestCar = null;
                int latestId = 1;

                if (carList != null && carList.Count > 0)
                    latestCar = carList.First() as Car;

                if (latestCar != null)
                    latestId = latestCar.Id + 1;

                Car car = new Car();
                car.Id = latestId;
                car.BrandId = int.Parse(comboBoxBrand.SelectedValue.ToString());
                car.CreationDate = DateTime.Now;
                car.Mileage = float.Parse(txtMileage.Text);
                car.ModelId = int.Parse(comboBoxModel.SelectedValue.ToString());
                car.Name = txtCarName.Text.ToString();
                car.TypeId = int.Parse(comboBoxType.SelectedValue.ToString());
                car.Year = int.Parse(txtYear.Text);
                car.PlateNumber = txtPlateNumber.Text;

                if (txtUpdateId.Text.ToString() == string.Empty)
                {
                    _carCollection.InsertOne(car);
                    MessageBox.Show("Vehicle inserted!", "Car Management", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else
                {
                    var update = Builders<BsonDocument>.Update.Set("BrandId", car.BrandId)
                        .Set("CreationDate", DateTime.Now)
                        .Set("Mileage", car.Mileage)
                        .Set("ModelId", car.ModelId)
                        .Set("Name", car.Name)
                        .Set("TypeId", car.TypeId)
                        .Set("Year", car.Year)
                        .Set("PlateNumber", car.PlateNumber);
                    //string updateString = "'PlateNumber' : 'ASD1000'";

                    //var carListUpdated = _carCollection.UpdateOneAsync(new BsonDocument("Id", 1), update);


                    //var filter = _carCollection.UpdateOne(new Builders<BsonDocument>().Filter.Eq("Id", latestId), update);


                    //_carCollection.UpdateOne(car);
                    MessageBox.Show("Vehicle updated!", "Car Management", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                
                ClearFields();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Something went wrong!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
