﻿using CarRental.Entity;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace CarRental
{
    public partial class CarManagement : Form
    {
        public int idToModify = 0;
        private IMongoDatabase _db;
        private IMongoCollection<Car> _carCollection;
        private IMongoCollection<CarBrand> _carBrandCollection;
        private IMongoCollection<CarType> _carTypeCollection;
        private IMongoCollection<CarModel> _carModelCollection;

        private static readonly string _selectDefaultValue = ConfigurationManager.AppSettings["selectDefaultValue"].ToString();

        public CarManagement()
        {
            InitializeComponent();
            dataGridViewCar.RowsAdded += DataGridViewCar_RowsAdded;

            _db = MongoDB.GetDatabase(ConfigurationManager.AppSettings["databaseName"].ToString());

            _carCollection = _db.GetCollection<Car>("Car");
            _carBrandCollection = _db.GetCollection<CarBrand>("CarBrand");
            _carTypeCollection = _db.GetCollection<CarType>("CarType");
            _carModelCollection = _db.GetCollection<CarModel>("CarModel");
        }

        private void btnFilter_Click(object sender, EventArgs e)
        {
            var stringFilter = "";

            BsonDocument filterBsonDocument = new BsonDocument();
            double mileageFrom = 0, mileageTo = 0;
            int yearFrom = 0, yearTo = 0;
            bool mileageFromOk = false, mileageToOk = false, yearFromOk = false, yearToOk = false;

            if (Double.TryParse(txtMileageFrom.Text, out mileageFrom))
                mileageFromOk = true;

            if (Double.TryParse(txtMileageTo.Text, out mileageTo))
                mileageToOk = true;

            if (int.TryParse(txtYearFrom.Text, out yearFrom))
                yearFromOk = true;

            if (int.TryParse(txtYearTo.Text, out yearTo))
                yearToOk = true;

            stringFilter = (mileageFromOk || mileageToOk ? stringFilter + " Mileage : ( {0} {1} )" : stringFilter);

            if (mileageFromOk && mileageToOk)
            {
                stringFilter = String.Format(stringFilter, "'$gte' : " + mileageFrom.ToString(), ", '$lte' : " + mileageTo.ToString()) + ", ";
            }
            else if (!mileageFromOk && mileageToOk)
            {
                stringFilter = String.Format(stringFilter, "", "'$lte' : " + mileageTo.ToString()) + ", ";
            }
            else if (mileageFromOk && !mileageToOk)
            {
                stringFilter = String.Format(stringFilter, "'$gte' : " + mileageFrom.ToString(), "") + ", ";
            }

            stringFilter += "{0}";

            if (comboBoxCar.SelectedIndex != 0)
            {
                stringFilter = String.Format(stringFilter, " Id : ( '$eq' : " + comboBoxCar.SelectedValue.ToString() + " )");
                stringFilter += ", {0}";
            }
            //filter = filter & builder.Eq("Id", 1);
            //filterBsonDocument.Add("Id", new BsonInt32(Int32.Parse(comboBoxCar.SelectedValue.ToString())));

            if (comboBoxCarBrand.SelectedIndex != 0)
            {
                stringFilter = String.Format(stringFilter, " BrandId : ( '$eq' : " + comboBoxCarBrand.SelectedValue.ToString() + " )");
                stringFilter += ", {0}";
            }
            //filter = filter & builder.Eq("BrandId", new BsonInt32(Int32.Parse(comboBoxCarBrand.SelectedValue.ToString())));
            //filterBsonDocument.Add("BrandId", new BsonInt32(Int32.Parse(comboBoxCarBrand.SelectedValue.ToString())));

            if (comboBoxCarType.SelectedIndex != 0)
            {
                stringFilter = String.Format(stringFilter, " TypeId : ( '$eq' : " + comboBoxCarType.SelectedValue.ToString() + " )");
                stringFilter += ", {0}";
            }
            //filter = filter & builder.Eq("TypeId", new BsonInt32(Int32.Parse(comboBoxCarType.SelectedValue.ToString())));
            //filterBsonDocument.Add("TypeId", new BsonInt32(Int32.Parse(comboBoxCarType.SelectedValue.ToString())));

            if (comboBoxCarModel.SelectedIndex != 0)
            {
                stringFilter = String.Format(stringFilter, " ModelId : ( '$eq' : " + comboBoxCarModel.SelectedValue.ToString() + " )");
                stringFilter += ", {0}";
            }
            //filter = filter & builder.Eq("ModelId", new BsonInt32(Int32.Parse(comboBoxCarModel.SelectedValue.ToString())));
            //filterBsonDocument.Add("ModelId", new BsonInt32(Int32.Parse(comboBoxCarModel.SelectedValue.ToString())));

            if (txtMileageFrom.Text.Trim() != "" && !Double.TryParse(txtMileageFrom.Text, out mileageFrom))
            {
                MessageBox.Show("Invalid Mileage!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                txtMileageFrom.Focus();
                return;
            }

            if (txtMileageTo.Text.Trim() != "" && !Double.TryParse(txtMileageTo.Text, out mileageFrom))
            {
                MessageBox.Show("Invalid Mileage!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                txtMileageTo.Focus();
                return;
            }

            if (txtYearFrom.Text.Trim() != "" && !int.TryParse(txtYearFrom.Text, out yearFrom))
            {
                MessageBox.Show("Invalid Year!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                txtYearFrom.Focus();
                return;
            }

            if (txtYearTo.Text.Trim() != "" && !int.TryParse(txtYearTo.Text, out yearTo))
            {
                MessageBox.Show("Invalid Mileage!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                txtYearTo.Focus();
                return;
            }

            stringFilter = (yearFromOk || yearToOk ? stringFilter.Replace(", {0}", ",").Replace("{0}", "") + " Year : ( {0} {1} )" : stringFilter);

            if (yearFromOk && yearToOk)
            {
                stringFilter = String.Format(stringFilter, "'$gte' : " + yearFrom.ToString(), ", '$lte' : " + yearTo.ToString()) + ", ";
                stringFilter += "{0}";
            }
            else if (!yearFromOk && yearToOk)
            {
                stringFilter = String.Format(stringFilter, "", "'$lte' : " + yearTo.ToString()) + ", ";
                stringFilter += "{0}";
            }
            else if (yearFromOk && !yearToOk)
            {
                stringFilter = String.Format(stringFilter, "'$gte' : " + yearFrom.ToString(), "") + ", ";
                stringFilter += "{0}";
            }

            stringFilter = stringFilter.Replace(", {0}", string.Empty).Replace(", {1}", string.Empty).Replace("{0}", string.Empty).Replace("(", "{").Replace(")", "}");
            stringFilter = "{ " + stringFilter + "}";

            var results = _carCollection.Find(stringFilter).Sort(new BsonDocument("Name", 1)).ToList();
            LoadGrid(results);
        }

        private void CarManagement_Load(object sender, EventArgs e)
        {
            LoadCar();
            LoadCarBrand();
            LoadCarType();
            LoadCarModel();
        }

        private void LoadCarModel()
        {
            var results = _carModelCollection.Find(new BsonDocument()).Sort(new BsonDocument("Name", 1)).ToList();

            results.Insert(0, new CarModel(0, _selectDefaultValue));
            LoadComboBox(comboBoxCarModel, results);
        }

        private void LoadCarType()
        {
            var results = _carTypeCollection.Find(new BsonDocument()).Sort(new BsonDocument("Description", 1)).ToList();

            results.Insert(0, new CarType(0, _selectDefaultValue));
            LoadComboBox(comboBoxCarType, results, "Description");
        }

        private void LoadCarBrand()
        {
            var results = _carBrandCollection.Find(new BsonDocument()).Sort(new BsonDocument("Name", 1)).ToList();

            results.Insert(0, new CarBrand(0, _selectDefaultValue));
            LoadComboBox(comboBoxCarBrand, results);
        }

        private void LoadCar()
        {
            var results = _carCollection.Find(new BsonDocument()).Sort(new BsonDocument("Name", 1)).ToList();
            var resultsToCombo = new List<Car>(results);

            resultsToCombo.Insert(0, new Car(0, _selectDefaultValue));
            LoadComboBox(comboBoxCar, resultsToCombo);

            LoadGrid(results);
        }

        private void LoadGrid<T>(List<T> dataSource)
        {
            dataGridViewCar.DataSource = dataSource;
            dataGridViewCar.Columns[0].Visible = false;
            dataGridViewCar.Columns[1].Visible = false;
        }

        private void DataGridViewCar_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            //MessageBox.Show((dataGridViewCar.Rows[e.RowIndex].DataBoundItem as Car).Name);
        }

        private void LoadComboBox<T>(ComboBox comboO, List<T> dataSource, string displayMember = "Name", string valueMember = "Id")
        {
            comboO.DataSource = dataSource;
            comboO.ValueMember = valueMember;
            comboO.DisplayMember = displayMember;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            CarManagement_Form carManForm = new CarManagement_Form();
            carManForm.ShowDialog();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            Car selected = dataGridViewCar.SelectedRows[0].DataBoundItem as Car;

            DialogResult result = MessageBox.Show("You will delete " + selected.Name + ". Continue?", "Delete", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            if (result.Equals(DialogResult.OK))
            {
                _carCollection.DeleteOne("{ Id : " + selected.Id + " }");
                var results = _carCollection.Find(new BsonDocument()).Sort(new BsonDocument("Name", 1)).ToList();
                LoadGrid(results);
            }
        }

        private void CarManagement_Shown(object sender, EventArgs e)
        {
            
        }

        private void CarManagement_Activated(object sender, EventArgs e)
        {
            LoadCar();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            Car selected = dataGridViewCar.SelectedRows[0].DataBoundItem as Car;

            CarManagement_Form carForm = new CarManagement_Form();
            carForm.txtUpdateId.Text = selected.Id.ToString();
            carForm.ShowDialog();
        }
    }
}