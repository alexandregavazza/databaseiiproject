﻿namespace CarRental
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.managementToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.carManagementToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rentalManagementToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clientManagementToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.officeManagementToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.managementToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(934, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // managementToolStripMenuItem
            // 
            this.managementToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.carManagementToolStripMenuItem,
            this.rentalManagementToolStripMenuItem,
            this.clientManagementToolStripMenuItem,
            this.officeManagementToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.managementToolStripMenuItem.Name = "managementToolStripMenuItem";
            this.managementToolStripMenuItem.Size = new System.Drawing.Size(90, 20);
            this.managementToolStripMenuItem.Text = "Management";
            // 
            // carManagementToolStripMenuItem
            // 
            this.carManagementToolStripMenuItem.Name = "carManagementToolStripMenuItem";
            this.carManagementToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.carManagementToolStripMenuItem.Text = "Car Management";
            this.carManagementToolStripMenuItem.Click += new System.EventHandler(this.carManagementToolStripMenuItem_Click);
            // 
            // rentalManagementToolStripMenuItem
            // 
            this.rentalManagementToolStripMenuItem.Name = "rentalManagementToolStripMenuItem";
            this.rentalManagementToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.rentalManagementToolStripMenuItem.Text = "Rental Management";
            this.rentalManagementToolStripMenuItem.Click += new System.EventHandler(this.rentalManagementToolStripMenuItem_Click);
            // 
            // clientManagementToolStripMenuItem
            // 
            this.clientManagementToolStripMenuItem.Name = "clientManagementToolStripMenuItem";
            this.clientManagementToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.clientManagementToolStripMenuItem.Text = "Client Management";
            this.clientManagementToolStripMenuItem.Click += new System.EventHandler(this.clientManagementToolStripMenuItem_Click);
            // 
            // officeManagementToolStripMenuItem
            // 
            this.officeManagementToolStripMenuItem.Name = "officeManagementToolStripMenuItem";
            this.officeManagementToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.officeManagementToolStripMenuItem.Text = "Office Management";
            this.officeManagementToolStripMenuItem.Click += new System.EventHandler(this.officeManagementToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.aboutToolStripMenuItem.Text = "About";
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(934, 654);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Main";
            this.Text = "Main";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem managementToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem carManagementToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rentalManagementToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clientManagementToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem officeManagementToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
    }
}