﻿namespace CarRental
{
    partial class Rentalmanagement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.Clientid = new System.Windows.Forms.Label();
            this.Carid = new System.Windows.Forms.Label();
            this.pickupid = new System.Windows.Forms.Label();
            this.returnid = new System.Windows.Forms.Label();
            this.discount = new System.Windows.Forms.Label();
            this.price = new System.Windows.Forms.Label();
            this.penalty = new System.Windows.Forms.Label();
            this.startdate = new System.Windows.Forms.Label();
            this.enddate = new System.Windows.Forms.Label();
            this.dateTimeEnd = new System.Windows.Forms.DateTimePicker();
            this.dateTimeStart = new System.Windows.Forms.DateTimePicker();
            this.textPickupid = new System.Windows.Forms.TextBox();
            this.textClientid = new System.Windows.Forms.TextBox();
            this.textCarid = new System.Windows.Forms.TextBox();
            this.textReturnid = new System.Windows.Forms.TextBox();
            this.comboPenalty = new System.Windows.Forms.ComboBox();
            this.checkedDiscount = new System.Windows.Forms.CheckedListBox();
            this.textPrice = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(242, 22);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Car Deals";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // Clientid
            // 
            this.Clientid.AutoSize = true;
            this.Clientid.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Clientid.Location = new System.Drawing.Point(48, 124);
            this.Clientid.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Clientid.Name = "Clientid";
            this.Clientid.Size = new System.Drawing.Size(50, 13);
            this.Clientid.TabIndex = 1;
            this.Clientid.Text = "Client ID:";
            // 
            // Carid
            // 
            this.Carid.AutoSize = true;
            this.Carid.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Carid.Location = new System.Drawing.Point(64, 85);
            this.Carid.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Carid.Name = "Carid";
            this.Carid.Size = new System.Drawing.Size(40, 13);
            this.Carid.TabIndex = 2;
            this.Carid.Text = "Car ID:";
            // 
            // pickupid
            // 
            this.pickupid.AutoSize = true;
            this.pickupid.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pickupid.Location = new System.Drawing.Point(3, 181);
            this.pickupid.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.pickupid.Name = "pickupid";
            this.pickupid.Size = new System.Drawing.Size(88, 13);
            this.pickupid.TabIndex = 3;
            this.pickupid.Text = "Pickup Office ID:";
            // 
            // returnid
            // 
            this.returnid.AutoSize = true;
            this.returnid.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.returnid.Location = new System.Drawing.Point(2, 232);
            this.returnid.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.returnid.Name = "returnid";
            this.returnid.Size = new System.Drawing.Size(87, 13);
            this.returnid.TabIndex = 4;
            this.returnid.Text = "Return Office ID:";
            this.returnid.Click += new System.EventHandler(this.label5_Click);
            // 
            // discount
            // 
            this.discount.AutoSize = true;
            this.discount.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.discount.Location = new System.Drawing.Point(24, 280);
            this.discount.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.discount.Name = "discount";
            this.discount.Size = new System.Drawing.Size(52, 13);
            this.discount.TabIndex = 5;
            this.discount.Text = "Discount:";
            this.discount.Click += new System.EventHandler(this.label6_Click);
            // 
            // price
            // 
            this.price.AutoSize = true;
            this.price.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.price.Location = new System.Drawing.Point(334, 130);
            this.price.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.price.Name = "price";
            this.price.Size = new System.Drawing.Size(34, 13);
            this.price.TabIndex = 7;
            this.price.Text = "Price:";
            // 
            // penalty
            // 
            this.penalty.AutoSize = true;
            this.penalty.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.penalty.Location = new System.Drawing.Point(319, 76);
            this.penalty.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.penalty.Name = "penalty";
            this.penalty.Size = new System.Drawing.Size(45, 13);
            this.penalty.TabIndex = 9;
            this.penalty.Text = "Penalty:";
            this.penalty.Click += new System.EventHandler(this.label10_Click);
            // 
            // startdate
            // 
            this.startdate.AutoSize = true;
            this.startdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.startdate.Location = new System.Drawing.Point(302, 181);
            this.startdate.Name = "startdate";
            this.startdate.Size = new System.Drawing.Size(58, 13);
            this.startdate.TabIndex = 11;
            this.startdate.Text = "Start Date:";
            // 
            // enddate
            // 
            this.enddate.AutoSize = true;
            this.enddate.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.enddate.Location = new System.Drawing.Point(307, 232);
            this.enddate.Name = "enddate";
            this.enddate.Size = new System.Drawing.Size(55, 13);
            this.enddate.TabIndex = 12;
            this.enddate.Text = "End Date:";
            // 
            // dateTimeEnd
            // 
            this.dateTimeEnd.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimeEnd.Location = new System.Drawing.Point(391, 223);
            this.dateTimeEnd.Name = "dateTimeEnd";
            this.dateTimeEnd.Size = new System.Drawing.Size(121, 19);
            this.dateTimeEnd.TabIndex = 13;
            // 
            // dateTimeStart
            // 
            this.dateTimeStart.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimeStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimeStart.Location = new System.Drawing.Point(391, 176);
            this.dateTimeStart.Name = "dateTimeStart";
            this.dateTimeStart.Size = new System.Drawing.Size(121, 19);
            this.dateTimeStart.TabIndex = 14;
            this.dateTimeStart.ValueChanged += new System.EventHandler(this.dateTimePicker2_ValueChanged);
            // 
            // textPickupid
            // 
            this.textPickupid.Location = new System.Drawing.Point(129, 177);
            this.textPickupid.Multiline = true;
            this.textPickupid.Name = "textPickupid";
            this.textPickupid.Size = new System.Drawing.Size(118, 26);
            this.textPickupid.TabIndex = 17;
            // 
            // textClientid
            // 
            this.textClientid.Location = new System.Drawing.Point(129, 124);
            this.textClientid.Multiline = true;
            this.textClientid.Name = "textClientid";
            this.textClientid.Size = new System.Drawing.Size(118, 26);
            this.textClientid.TabIndex = 18;
            // 
            // textCarid
            // 
            this.textCarid.Location = new System.Drawing.Point(129, 76);
            this.textCarid.Multiline = true;
            this.textCarid.Name = "textCarid";
            this.textCarid.Size = new System.Drawing.Size(118, 26);
            this.textCarid.TabIndex = 19;
            // 
            // textReturnid
            // 
            this.textReturnid.Location = new System.Drawing.Point(129, 223);
            this.textReturnid.Multiline = true;
            this.textReturnid.Name = "textReturnid";
            this.textReturnid.Size = new System.Drawing.Size(118, 26);
            this.textReturnid.TabIndex = 20;
            // 
            // comboPenalty
            // 
            this.comboPenalty.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboPenalty.FormattingEnabled = true;
            this.comboPenalty.Items.AddRange(new object[] {
            "5$ for one hour",
            "15$ for two hours",
            "30$ for three hours",
            "45$ for four hours",
            "price*2 after four hours"});
            this.comboPenalty.Location = new System.Drawing.Point(391, 73);
            this.comboPenalty.Name = "comboPenalty";
            this.comboPenalty.Size = new System.Drawing.Size(121, 21);
            this.comboPenalty.TabIndex = 21;
            // 
            // checkedDiscount
            // 
            this.checkedDiscount.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkedDiscount.FormattingEnabled = true;
            this.checkedDiscount.Items.AddRange(new object[] {
            "5% for 3 hours",
            "10% for 5 hours",
            "25% for 8 hours",
            "35% for 24 hours",
            "40% for 48 hours"});
            this.checkedDiscount.Location = new System.Drawing.Point(127, 280);
            this.checkedDiscount.Name = "checkedDiscount";
            this.checkedDiscount.Size = new System.Drawing.Size(120, 60);
            this.checkedDiscount.TabIndex = 22;
            // 
            // textPrice
            // 
            this.textPrice.Location = new System.Drawing.Point(391, 124);
            this.textPrice.Multiline = true;
            this.textPrice.Name = "textPrice";
            this.textPrice.Size = new System.Drawing.Size(121, 26);
            this.textPrice.TabIndex = 23;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(274, 342);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(79, 23);
            this.button1.TabIndex = 24;
            this.button1.Text = "Submit";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(359, 342);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(77, 23);
            this.button3.TabIndex = 28;
            this.button3.Text = "Clear";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Location = new System.Drawing.Point(442, 342);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(80, 23);
            this.button4.TabIndex = 29;
            this.button4.Text = "Exit";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // Rentalmanagement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 389);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textPrice);
            this.Controls.Add(this.checkedDiscount);
            this.Controls.Add(this.comboPenalty);
            this.Controls.Add(this.textReturnid);
            this.Controls.Add(this.textCarid);
            this.Controls.Add(this.textClientid);
            this.Controls.Add(this.textPickupid);
            this.Controls.Add(this.dateTimeStart);
            this.Controls.Add(this.dateTimeEnd);
            this.Controls.Add(this.enddate);
            this.Controls.Add(this.startdate);
            this.Controls.Add(this.penalty);
            this.Controls.Add(this.price);
            this.Controls.Add(this.discount);
            this.Controls.Add(this.returnid);
            this.Controls.Add(this.pickupid);
            this.Controls.Add(this.Carid);
            this.Controls.Add(this.Clientid);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Rentalmanagement";
            this.Text = "Rentalmanagement";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label Clientid;
        private System.Windows.Forms.Label Carid;
        private System.Windows.Forms.Label pickupid;
        private System.Windows.Forms.Label returnid;
        private System.Windows.Forms.Label discount;
        private System.Windows.Forms.Label price;
        private System.Windows.Forms.Label penalty;
        private System.Windows.Forms.Label startdate;
        private System.Windows.Forms.Label enddate;
        private System.Windows.Forms.DateTimePicker dateTimeEnd;
        private System.Windows.Forms.DateTimePicker dateTimeStart;
        private System.Windows.Forms.TextBox textPickupid;
        private System.Windows.Forms.TextBox textClientid;
        private System.Windows.Forms.TextBox textCarid;
        private System.Windows.Forms.TextBox textReturnid;
        private System.Windows.Forms.ComboBox comboPenalty;
        private System.Windows.Forms.CheckedListBox checkedDiscount;
        private System.Windows.Forms.TextBox textPrice;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
    }
}