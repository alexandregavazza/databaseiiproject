﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CarRental
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
        }

        private void carManagementToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CarManagement carManagement = new CarManagement();
            carManagement.ShowDialog();
           
        }

        private void rentalManagementToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RentalManagement_Form rentalmanagement = new RentalManagement_Form();
            rentalmanagement.ShowDialog();
        }

        private void clientManagementToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ClientManagement_Form clientManagement = new ClientManagement_Form();
            clientManagement.ShowDialog();
        }

        private void officeManagementToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OfficeManagement_Form officeManagement = new OfficeManagement_Form();
            officeManagement.ShowDialog();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
