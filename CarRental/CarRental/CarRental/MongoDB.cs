﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;
using System.Configuration;

namespace CarRental
{
    public class MongoDB
    {
        private static readonly string _connectionString = ConfigurationManager.ConnectionStrings["mongoDBConnectionString"].ToString();
        private static IMongoClient _client;

        public static IMongoClient GetClient()
        {
            if (_client == null)
                _client = new MongoClient(new MongoUrl(_connectionString));

            return _client;
        }

        public static IMongoDatabase GetDatabase(string databaseName)
        {
            return GetClient().GetDatabase(databaseName);
        }
    }
}
