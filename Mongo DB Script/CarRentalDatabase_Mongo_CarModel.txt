db.CarModel.insertMany([
	{ Id : 1, Name : "3 Series", CreationDate : new Date() },
	{ Id : 2, Name : "5 Series", CreationDate : new Date() },
	{ Id : 3, Name : "7 Series", CreationDate : new Date() },
	{ Id : 4, Name : "X Series", CreationDate : new Date() },
	{ Id : 5, Name : "A3", CreationDate : new Date() },
	{ Id : 6, Name : "A4", CreationDate : new Date() },
	{ Id : 7, Name : "A5", CreationDate : new Date() },
	{ Id : 8, Name : "A6", CreationDate : new Date() },
	{ Id : 9, Name : "C Class", CreationDate : new Date() },
	{ Id : 10, Name : "E Class", CreationDate : new Date() },
	{ Id : 11, Name : "S Class", CreationDate : new Date() },
	{ Id : 12, Name : "F Series", CreationDate : new Date() },
	{ Id : 13, Name : "911", CreationDate : new Date() },
	{ Id : 14, Name : "Corolla", CreationDate : new Date() },
	{ Id : 15, Name : "Camaro", CreationDate : new Date() }
]);