db.CarType.insertMany([
	{ Id : 1, Description : "SUV", CreationDate : new Date() },
	{ Id : 2, Description : "Hatchback", CreationDate : new Date() },
	{ Id : 3, Description : "Sedan", CreationDate : new Date() },
	{ Id : 4, Description : "SW", CreationDate : new Date() },
	{ Id : 5, Description : "Pickup Truck", CreationDate : new Date() },
	{ Id : 6, Description : "Coupe", CreationDate : new Date() }
]);