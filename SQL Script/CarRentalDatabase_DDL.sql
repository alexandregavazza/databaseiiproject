USE [master]
GO

/****** Object:  Database [CarRental]    Script Date: 6/8/2019 9:19:44 AM ******/
CREATE DATABASE [CarRental]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'CarRental', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\CarRental.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'CarRental_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\CarRental_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO

ALTER DATABASE [CarRental] SET COMPATIBILITY_LEVEL = 140
GO

IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [CarRental].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO





--asdassd


ALTER DATABASE [CarRental] SET ANSI_NULL_DEFAULT OFF 
GO

ALTER DATABASE [CarRental] SET ANSI_NULLS OFF 
GO

ALTER DATABASE [CarRental] SET ANSI_PADDING OFF 
GO

ALTER DATABASE [CarRental] SET ANSI_WARNINGS OFF 
GO

ALTER DATABASE [CarRental] SET ARITHABORT OFF 
GO

ALTER DATABASE [CarRental] SET AUTO_CLOSE OFF 
GO

ALTER DATABASE [CarRental] SET AUTO_SHRINK OFF 
GO

ALTER DATABASE [CarRental] SET AUTO_UPDATE_STATISTICS ON 
GO

ALTER DATABASE [CarRental] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO

ALTER DATABASE [CarRental] SET CURSOR_DEFAULT  GLOBAL 
GO

ALTER DATABASE [CarRental] SET CONCAT_NULL_YIELDS_NULL OFF 
GO

ALTER DATABASE [CarRental] SET NUMERIC_ROUNDABORT OFF 
GO

ALTER DATABASE [CarRental] SET QUOTED_IDENTIFIER OFF 
GO

ALTER DATABASE [CarRental] SET RECURSIVE_TRIGGERS OFF 
GO

ALTER DATABASE [CarRental] SET  DISABLE_BROKER 
GO

ALTER DATABASE [CarRental] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO

ALTER DATABASE [CarRental] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO

ALTER DATABASE [CarRental] SET TRUSTWORTHY OFF 
GO

ALTER DATABASE [CarRental] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO

ALTER DATABASE [CarRental] SET PARAMETERIZATION SIMPLE 
GO

ALTER DATABASE [CarRental] SET READ_COMMITTED_SNAPSHOT OFF 
GO

ALTER DATABASE [CarRental] SET HONOR_BROKER_PRIORITY OFF 
GO

ALTER DATABASE [CarRental] SET RECOVERY FULL 
GO

ALTER DATABASE [CarRental] SET  MULTI_USER 
GO

ALTER DATABASE [CarRental] SET PAGE_VERIFY CHECKSUM  
GO

ALTER DATABASE [CarRental] SET DB_CHAINING OFF 
GO

ALTER DATABASE [CarRental] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO

ALTER DATABASE [CarRental] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO

ALTER DATABASE [CarRental] SET DELAYED_DURABILITY = DISABLED 
GO

ALTER DATABASE [CarRental] SET QUERY_STORE = OFF
GO

ALTER DATABASE [CarRental] SET  READ_WRITE 
GO


USE [CarRental]
GO

/****** Object:  Table [dbo].[CarBrand]    Script Date: 6/4/2019 2:52:05 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CarBrand](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nchar](10) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
 CONSTRAINT [PK_CarBrand] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[CarBrand] ADD  CONSTRAINT [DF_CarBrand_CreationDate]  DEFAULT (getdate()) FOR [CreationDate]
GO


USE [CarRental]
GO

/****** Object:  Table [dbo].[Office]    Script Date: 6/4/2019 3:10:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Office](
	[Id] [int] NOT NULL,
	[Name] [varchar](128) NOT NULL,
	[AddressId] [int] NOT NULL,
	[CreationDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Office] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Office] ADD  CONSTRAINT [DF_Office_CreationDate]  DEFAULT (getdate()) FOR [CreationDate]
GO


USE [CarRental]
GO

/****** Object:  Table [dbo].[Client]    Script Date: 6/4/2019 3:01:06 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Client](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](128) NOT NULL,
	[PassportNumber] [char](8) NOT NULL,
	[PhoneNumber] [varchar](16) NOT NULL,
	[CelularNumber] [varchar](16) NOT NULL,
	[Citizenship] [varchar](64) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Client] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Client] ADD  CONSTRAINT [DF_Client_CreationDate]  DEFAULT (getdate()) FOR [CreationDate]
GO


USE [CarRental]
GO

/****** Object:  Table [dbo].[CarType]    Script Date: 6/4/2019 2:52:12 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CarType](
	[Id] [int] NOT NULL,
	[Description] [varchar](64) NOT NULL,
	[CreationDate] [datetime] NULL,
 CONSTRAINT [PK_CarType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[CarType] ADD  CONSTRAINT [DF_CarType_CreationDate]  DEFAULT (getdate()) FOR [CreationDate]
GO


USE [CarRental]
GO

/****** Object:  Table [dbo].[CarModel]    Script Date: 6/4/2019 2:52:09 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CarModel](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](64) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Model] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[CarModel] ADD  CONSTRAINT [DF_CarModel_CreationDate]  DEFAULT (getdate()) FOR [CreationDate]
GO


USE [CarRental]
GO

/****** Object:  Table [dbo].[Address]    Script Date: 6/4/2019 3:01:02 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Address](
	[Id] [int] NOT NULL,
	[ClientId] [int] NOT NULL,
	[Street] [varchar](128) NOT NULL,
	[Number] [int] NOT NULL,
	[Complement] [varchar](128) NOT NULL,
	[City] [varchar](128) NOT NULL,
	[State] [varchar](64) NOT NULL,
	[Country] [varchar](64) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Address] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Address] ADD  CONSTRAINT [DF_Address_CreationDate]  DEFAULT (getdate()) FOR [CreationDate]
GO

ALTER TABLE [dbo].[Address]  WITH CHECK ADD  CONSTRAINT [FK_Address_Client] FOREIGN KEY([ClientId])
REFERENCES [dbo].[Client] ([Id])
GO

ALTER TABLE [dbo].[Address] CHECK CONSTRAINT [FK_Address_Client]
GO


USE [CarRental]
GO

/****** Object:  Table [dbo].[Car]    Script Date: 6/4/2019 2:51:59 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Car](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](64) NOT NULL,
	[ModelId] [int] NOT NULL,
	[BrandId] [int] NOT NULL,
	[Mileage] [int] NOT NULL,
	[TypeId] [int] NOT NULL,
	[Year] [int] NOT NULL,
	[CreationDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Car] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Car] ADD  CONSTRAINT [DF_Car_CreationDate]  DEFAULT (getdate()) FOR [CreationDate]
GO

ALTER TABLE [dbo].[Car]  WITH CHECK ADD  CONSTRAINT [FK_Car_CarBrand] FOREIGN KEY([BrandId])
REFERENCES [dbo].[CarBrand] ([Id])
GO

ALTER TABLE [dbo].[Car] CHECK CONSTRAINT [FK_Car_CarBrand]
GO

ALTER TABLE [dbo].[Car]  WITH CHECK ADD  CONSTRAINT [FK_Car_CarModel] FOREIGN KEY([ModelId])
REFERENCES [dbo].[CarModel] ([Id])
GO

ALTER TABLE [dbo].[Car] CHECK CONSTRAINT [FK_Car_CarModel]
GO

ALTER TABLE [dbo].[Car]  WITH CHECK ADD  CONSTRAINT [FK_Car_CarType] FOREIGN KEY([TypeId])
REFERENCES [dbo].[CarType] ([Id])
GO

ALTER TABLE [dbo].[Car] CHECK CONSTRAINT [FK_Car_CarType]
GO


USE [CarRental]
GO

/****** Object:  Table [dbo].[Rent]    Script Date: 6/4/2019 3:10:25 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Rent](
	[Id] [int] NOT NULL,
	[CarId] [int] NOT NULL,
	[ClientId] [int] NOT NULL,
	[PickupOfficeId] [int] NOT NULL,
	[ReturnOfficeId] [int] NOT NULL,
	[Price] [decimal](18, 0) NOT NULL,
	[Discount] [decimal](18, 0) NOT NULL,
	[Penalty] [decimal](18, 0) NOT NULL,
	[TotalPrice] [decimal](18, 0) NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NOT NULL,
	[CreationDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Rent] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Rent] ADD  CONSTRAINT [DF_Rent_Price]  DEFAULT ((0)) FOR [Price]
GO

ALTER TABLE [dbo].[Rent] ADD  CONSTRAINT [DF_Rent_Discount]  DEFAULT ((0)) FOR [Discount]
GO

ALTER TABLE [dbo].[Rent] ADD  CONSTRAINT [DF_Rent_Penalty]  DEFAULT ((0)) FOR [Penalty]
GO

ALTER TABLE [dbo].[Rent] ADD  CONSTRAINT [DF_Rent_TotalPrice]  DEFAULT ((0)) FOR [TotalPrice]
GO

ALTER TABLE [dbo].[Rent] ADD  CONSTRAINT [DF_Rent_CreationDate]  DEFAULT (getdate()) FOR [CreationDate]
GO

ALTER TABLE [dbo].[Rent]  WITH CHECK ADD  CONSTRAINT [FK_Rent_Car] FOREIGN KEY([CarId])
REFERENCES [dbo].[Car] ([Id])
GO

ALTER TABLE [dbo].[Rent] CHECK CONSTRAINT [FK_Rent_Car]
GO

ALTER TABLE [dbo].[Rent]  WITH CHECK ADD  CONSTRAINT [FK_Rent_Client] FOREIGN KEY([ClientId])
REFERENCES [dbo].[Client] ([Id])
GO

ALTER TABLE [dbo].[Rent] CHECK CONSTRAINT [FK_Rent_Client]
GO

ALTER TABLE [dbo].[Rent]  WITH CHECK ADD  CONSTRAINT [FK_Rent_PickupOffice] FOREIGN KEY([PickupOfficeId])
REFERENCES [dbo].[Office] ([Id])
GO

ALTER TABLE [dbo].[Rent] CHECK CONSTRAINT [FK_Rent_PickupOffice]
GO

ALTER TABLE [dbo].[Rent]  WITH CHECK ADD  CONSTRAINT [FK_Rent_ReturnOffice] FOREIGN KEY([ReturnOfficeId])
REFERENCES [dbo].[Office] ([Id])
GO

ALTER TABLE [dbo].[Rent] CHECK CONSTRAINT [FK_Rent_ReturnOffice]
GO